package com.example.safa.findme;

public class Position {
    public String lat;
    public String lon;
    public String id;

    public Position(String lat, String lon, String id) {
        this.lat = lat;
        this.lon = lon;
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getId() {
        return id;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Position{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
