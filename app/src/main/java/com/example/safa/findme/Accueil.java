package com.example.safa.findme;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Accueil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                PositionHelper ph=new PositionHelper(Accueil.this,PositionHelper.namee,null,PositionHelper.version);
                SQLiteDatabase bd=ph.getWritableDatabase();
                ContentValues values=new ContentValues();
            values.put(PositionHelper.col_num,"20307291");
            values.put(PositionHelper.col_lat,"40");
            values.put(PositionHelper.col_lon,"40");
                Long a=bd.insert(PositionHelper.tablee,null,values);
if(a>0){
    Toast.makeText(Accueil.this, "succes", Toast.LENGTH_SHORT).show();}

            else{
                Toast.makeText(Accueil.this, "Echec", Toast.LENGTH_SHORT).show();
    ArrayList<Position> data=new ArrayList<Position>();

    Cursor c=bd.query(PositionHelper.tablee,new String[]{PositionHelper.col_Id,PositionHelper.col_num,PositionHelper.col_lat,PositionHelper.col_lon},null,null,null,null,null,null);
  c.moveToFirst();
  data.clear();
while (!c.isAfterLast()){
String i=c.getString(0);
String s=c.getString(1);
String s2=c.getString(2);
Position p=new Position(i,s,s2);
data.add(p);
c.moveToNext();
ArrayAdapter t= new ArrayAdapter(Accueil.this,android.R.layout.simple_list_item_1,data);
    ListView lv=(ListView)findViewById(R.id.lview);
    lv.setAdapter(t);
}
               }
            }
        });

}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accueil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
