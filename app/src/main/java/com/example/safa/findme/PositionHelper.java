package com.example.safa.findme;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PositionHelper extends SQLiteOpenHelper{
    public static final String tablee="Position" ,col_Id="Identifiant";
    public static final String col_num="Numero" ,col_lon="Longitude";
    public static final String col_lat="Latitude" ;
    public  static String namee="position.db";
    public static int version=1;
    String req="create table "+tablee+"("+col_Id+" integer primary key AutoIncrement,"+col_num+" Text Not Null,"+col_lon+" Text Not Null,"+col_lat+" Text Not Null );";

    public PositionHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
sqLiteDatabase.execSQL(req);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    sqLiteDatabase.execSQL("Drop Table "+tablee);
    sqLiteDatabase.execSQL(req);
    }
}
